package com.example.shawnli.pets.injection.modules

import android.app.Application
import android.content.Context
import com.example.shawnli.pets.data.managers.PetOwnerManager
import com.example.shawnli.pets.data.sources.PetOwnerClient
import com.example.shawnli.pets.domain.repositories.PetOwnerRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun providePetOwnerRepository(client: PetOwnerClient): PetOwnerRepository = PetOwnerManager(client)
}