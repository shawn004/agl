package com.example.shawnli.pets.data.responses

data class PetResponse(
    val name: String,
    val type: String
)