package com.example.shawnli.pets.data.sources

import com.example.shawnli.pets.data.responses.PetOwnerResponse
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class PetOwnerClient @Inject constructor() {

    companion object {
        private const val BASE_URL = "http://agl-developer-test.azurewebsites.net"
    }

    private var service: PetOwnerService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
        service = retrofit.create(PetOwnerService::class.java)
    }

    open fun getPetOwners(): Single<List<PetOwnerResponse>> =
        service.getPetOwners()
            .subscribeOn(Schedulers.io())
}