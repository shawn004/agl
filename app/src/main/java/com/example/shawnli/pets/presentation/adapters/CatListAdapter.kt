package com.example.shawnli.pets.presentation.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.shawnli.pets.R
import kotlinx.android.synthetic.main.item_header.view.*
import kotlinx.android.synthetic.main.item_pet.view.*

class CatListAdapter(private var data: List<CatListItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_CAT = 1
    }

    fun update(data: List<CatListItem>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.count()

    override fun getItemViewType(position: Int): Int =
        if (data[position] is HeaderItem) TYPE_HEADER else TYPE_CAT

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            TYPE_HEADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
                HeaderViewHolder(view)
            }
            TYPE_CAT -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pet, parent, false)
                ItemViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown view type")
        }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]
        when (viewHolder.itemViewType) {
            TYPE_HEADER -> (viewHolder as HeaderViewHolder).headerTextView.text = item.text
            TYPE_CAT -> (viewHolder as ItemViewHolder).nameTextView.text = item.text
        }
    }

    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val headerTextView: TextView = view.header
    }

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.petName
    }
}