package com.example.shawnli.pets.presentation.adapters

sealed class CatListItem(open val text: String)

data class HeaderItem(override val text: String) : CatListItem(text)
data class CatItem(override val text: String) : CatListItem(text)