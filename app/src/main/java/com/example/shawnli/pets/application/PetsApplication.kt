package com.example.shawnli.pets.application

import android.app.Application
import com.example.shawnli.pets.injection.components.ApplicationComponent
import com.example.shawnli.pets.injection.components.DaggerApplicationComponent
import com.example.shawnli.pets.injection.modules.ApplicationModule


class PetsApplication: Application() {

    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initApplicationComponent()
    }

    private fun initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)
    }
}