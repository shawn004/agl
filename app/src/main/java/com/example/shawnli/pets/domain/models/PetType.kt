package com.example.shawnli.pets.domain.models

enum class PetType {
    CAT, DOG, FISH, OTHER;

    companion object {
        fun from(response: String): PetType =
            try {
                PetType.valueOf(response.toUpperCase())
            } catch (e: IllegalArgumentException) {
                OTHER
            }
    }
}