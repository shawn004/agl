package com.example.shawnli.pets.presentation.activities

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.shawnli.pets.R
import com.example.shawnli.pets.application.PetsApplication
import com.example.shawnli.pets.injection.components.DaggerActivityComponent
import com.example.shawnli.pets.presentation.adapters.CatListAdapter
import com.example.shawnli.pets.presentation.presenters.CatsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_pets.*
import javax.inject.Inject

class CatsActivity : AppCompatActivity() {

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CatsViewModel

    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pets)

        DaggerActivityComponent
            .builder()
            .applicationComponent(PetsApplication.applicationComponent)
            .build()
            .inject(this)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CatsViewModel::class.java)

        setupUI()
    }

    override fun onStart() {
        super.onStart()
        fetchCats()
    }

    override fun onStop() {
        disposables.clear()
        super.onStop()
    }

    private fun setupUI() {
        catsRecyclerView.adapter = CatListAdapter(emptyList())
        catsRecyclerView.layoutManager = LinearLayoutManager(this)
        catsRecyclerView.setHasFixedSize(true)
    }

    private fun fetchCats() {
        disposables.add(
            viewModel.getData()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressBar.visibility = View.VISIBLE }
                .doAfterTerminate { progressBar.visibility = View.GONE }
                .subscribe(
                    {
                        (catsRecyclerView.adapter as CatListAdapter).update(it)
                    },
                    {
                        it.printStackTrace()
                    }
                )

        )
    }
}
