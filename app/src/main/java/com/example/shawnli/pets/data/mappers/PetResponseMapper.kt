package com.example.shawnli.pets.data.mappers

import com.example.shawnli.pets.data.responses.PetResponse
import com.example.shawnli.pets.domain.models.Pet
import com.example.shawnli.pets.domain.models.PetType

fun mapToPet(response: PetResponse): Pet =
    with(response) {
        Pet(
            name = name,
            type = PetType.from(type)
        )
    }
