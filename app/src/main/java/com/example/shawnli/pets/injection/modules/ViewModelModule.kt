package com.example.shawnli.pets.injection.modules

import android.arch.lifecycle.ViewModel
import com.example.shawnli.pets.injection.annotations.ViewModelKey
import com.example.shawnli.pets.presentation.presenters.CatsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CatsViewModel::class)
    abstract fun bindCatsViewModel(viewModel: CatsViewModel): ViewModel
}