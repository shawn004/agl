package com.example.shawnli.pets.data.managers

import com.example.shawnli.pets.data.mappers.mapToPetOwner
import com.example.shawnli.pets.data.sources.PetOwnerClient
import com.example.shawnli.pets.domain.models.PetOwner
import com.example.shawnli.pets.domain.repositories.PetOwnerRepository
import io.reactivex.Single

class PetOwnerManager(private val petOwnerClient: PetOwnerClient) : PetOwnerRepository {

    override fun getPetOwners(): Single<List<PetOwner>> =
        petOwnerClient.getPetOwners().map { response ->
            response.map { mapToPetOwner(it) }
        }
}