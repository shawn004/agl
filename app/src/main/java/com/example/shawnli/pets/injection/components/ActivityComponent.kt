package com.example.shawnli.pets.injection.components

import com.example.shawnli.pets.presentation.activities.CatsActivity
import com.example.shawnli.pets.injection.modules.ActivityModule
import com.example.shawnli.pets.injection.annotations.PerActivity
import com.example.shawnli.pets.injection.modules.ViewModelFactoryModule
import com.example.shawnli.pets.injection.modules.ViewModelModule
import dagger.Component

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ActivityModule::class, ViewModelModule::class, ViewModelFactoryModule::class]
)
interface ActivityComponent {

    fun inject(activity: CatsActivity)
}