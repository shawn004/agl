package com.example.shawnli.pets.data.mappers

import com.example.shawnli.pets.data.responses.PetOwnerResponse
import com.example.shawnli.pets.domain.models.Gender
import com.example.shawnli.pets.domain.models.PetOwner

fun mapToPetOwner(response: PetOwnerResponse): PetOwner =
    with(response) {
        PetOwner(
            name = name,
            gender = Gender.from(gender),
            age = age,
            pets = pets?.map { mapToPet(it) } ?: emptyList()
        )
    }
