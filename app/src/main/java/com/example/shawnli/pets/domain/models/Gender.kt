package com.example.shawnli.pets.domain.models

enum class Gender {
    MALE, FEMALE, OTHER;

    companion object {
        fun from(response: String): Gender =
            try {
                Gender.valueOf(response.toUpperCase())
            } catch (e: IllegalArgumentException) {
                OTHER
            }
    }
}