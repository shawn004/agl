package com.example.shawnli.pets.domain.repositories

import com.example.shawnli.pets.domain.models.PetOwner
import io.reactivex.Single

interface PetOwnerRepository {
    fun getPetOwners(): Single<List<PetOwner>>
}