package com.example.shawnli.pets.domain.models

data class Pet(
    val name: String,
    val type: PetType
)