package com.example.shawnli.pets.data.responses

data class PetOwnerResponse(
    val name: String,
    val gender: String,
    val age: Int,
    val pets: List<PetResponse>?
)