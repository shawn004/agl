package com.example.shawnli.pets.domain.models

data class PetOwner(
    val name: String,
    val gender: Gender,
    val age: Int,
    val pets: List<Pet> = emptyList()
)