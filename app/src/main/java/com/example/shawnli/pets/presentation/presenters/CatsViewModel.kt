package com.example.shawnli.pets.presentation.presenters

import android.arch.lifecycle.ViewModel
import com.example.shawnli.pets.domain.models.PetType
import com.example.shawnli.pets.domain.repositories.PetOwnerRepository
import com.example.shawnli.pets.injection.annotations.PerActivity
import com.example.shawnli.pets.presentation.adapters.CatItem
import com.example.shawnli.pets.presentation.adapters.CatListItem
import com.example.shawnli.pets.presentation.adapters.HeaderItem
import io.reactivex.Single
import javax.inject.Inject

@PerActivity
class CatsViewModel @Inject constructor(private val repository: PetOwnerRepository) : ViewModel() {

    private var cache: List<CatListItem>? = null

    // fetch fresh data if in-memory cache doesn't exist
    fun getData(): Single<List<CatListItem>> = cache?.let { Single.just(it) } ?: getFresh()

    private fun getFresh(): Single<List<CatListItem>> =
        repository.getPetOwners()
            .map { owners ->
                cache = owners.groupBy { it.gender }.flatMap { (gender, owners) ->
                    val catItems = owners.flatMap { it.pets }
                        .filter { it.type == PetType.CAT }
                        .sortedBy { it.name }
                        .map { CatItem(it.name) }

                    listOf(HeaderItem(gender.name)) + catItems
                }
                cache
            }

}