package com.example.shawnli.pets.injection.components

import android.content.Context
import com.example.shawnli.pets.application.PetsApplication
import com.example.shawnli.pets.domain.repositories.PetOwnerRepository
import com.example.shawnli.pets.injection.modules.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    val context: Context

    val petOwnerRepository: PetOwnerRepository

    fun inject(application: PetsApplication)
}