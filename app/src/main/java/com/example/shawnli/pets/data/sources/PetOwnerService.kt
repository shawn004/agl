package com.example.shawnli.pets.data.sources

import com.example.shawnli.pets.data.responses.PetOwnerResponse
import io.reactivex.Single
import retrofit2.http.GET

interface PetOwnerService {

    @GET("/people.json")
    fun getPetOwners(): Single<List<PetOwnerResponse>>
}