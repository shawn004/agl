package com.example.shawnli.pets.factories

import com.example.shawnli.pets.domain.models.Pet
import com.example.shawnli.pets.domain.models.PetType

class PetFactory {

    companion object {
        fun createPet(name: String = "Mocha", type: PetType = PetType.DOG) = Pet(name, type)
    }
}

