package com.example.shawnli.pets

import com.example.shawnli.pets.domain.models.Gender
import com.example.shawnli.pets.domain.models.PetType
import com.example.shawnli.pets.domain.repositories.PetOwnerRepository
import com.example.shawnli.pets.factories.PetFactory
import com.example.shawnli.pets.factories.PetOwnerFactory
import com.example.shawnli.pets.presentation.adapters.CatItem
import com.example.shawnli.pets.presentation.adapters.HeaderItem
import com.example.shawnli.pets.presentation.presenters.CatsViewModel
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.kotlintest.specs.FunSpec
import io.reactivex.Single
import org.mockito.internal.verification.Times

class CatsViewModelTest : FunSpec() {

    private val petOwners = listOf(
        PetOwnerFactory.create(
            gender = Gender.MALE, pets = listOf(
                PetFactory.createPet(
                    name = "Oreo",
                    type = PetType.CAT
                )
            )
        ),
        PetOwnerFactory.create(
            gender = Gender.FEMALE, pets = listOf(
                PetFactory.createPet(
                    name = "Choc",
                    type = PetType.DOG
                ),
                PetFactory.createPet(
                    name = "Mint",
                    type = PetType.CAT
                )
            )
        ),
        PetOwnerFactory.create(
            gender = Gender.MALE, pets = listOf(
                PetFactory.createPet(
                    name = "Coffee",
                    type = PetType.OTHER
                ),
                PetFactory.createPet(
                    name = "Latte",
                    type = PetType.CAT
                )
            )
        )
    )

    init {

        test("getData() returns a lit of cat list items") {
            // given
            val repositoryMock: PetOwnerRepository = mock {
                on(mock.getPetOwners()) doReturn Single.just(petOwners)
            }
            val viewModel = CatsViewModel(repositoryMock)

            // when
            val testObserver = viewModel.getData().test()
            testObserver.awaitTerminalEvent()

            // then
            testObserver
                .assertNoErrors()
                .assertValue {
                    it[0] == HeaderItem("MALE") &&
                            it[1] == CatItem("Latte") &&
                            it[2] == CatItem("Oreo") &&
                            it[3] == HeaderItem("FEMALE") &&
                            it[4] == CatItem("Mint")
                }
        }

        test("getData() only retrieves fresh data once") {
            // given
            val repositoryMock: PetOwnerRepository = mock {
                on(mock.getPetOwners()) doReturn Single.just(petOwners)
            }
            val viewModel = CatsViewModel(repositoryMock)

            // when
            val testObserver1 = viewModel.getData().test()
            testObserver1.awaitTerminalEvent()

            val testObserver2 = viewModel.getData().test()
            testObserver2.awaitTerminalEvent()

            val testObserver3 = viewModel.getData().test()
            testObserver3.awaitTerminalEvent()

            // then
            verify(repositoryMock, Times(1)).getPetOwners()
        }
    }
}