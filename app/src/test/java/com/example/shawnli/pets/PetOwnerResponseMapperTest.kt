package com.example.shawnli.pets

import com.example.shawnli.pets.data.mappers.mapToPetOwner
import com.example.shawnli.pets.data.responses.PetOwnerResponse
import com.example.shawnli.pets.domain.models.Gender
import com.example.shawnli.pets.domain.models.PetOwner
import io.kotlintest.shouldBe
import io.kotlintest.specs.FunSpec
import io.kotlintest.tables.Headers2
import io.kotlintest.tables.forAll
import io.kotlintest.tables.row
import io.kotlintest.tables.table

class PetOwnerResponseMapperTest : FunSpec() {

    private val table = table(
        Headers2("response", "expected"),
        row(PetOwnerResponse("Peter", "male", 50, null), PetOwner("Peter", Gender.MALE, 50, emptyList())),
        row(PetOwnerResponse("Peter", "MALE", 50, null), PetOwner("Peter", Gender.MALE, 50, emptyList())),
        row(PetOwnerResponse("Peter", "MALE", 50, null), PetOwner("Peter", Gender.MALE, 50, emptyList())),
        row(PetOwnerResponse("Kate", "female", 0, null), PetOwner("Kate", Gender.FEMALE, 0, emptyList())),
        row(PetOwnerResponse("Kate", "other", 1000, null), PetOwner("Kate", Gender.OTHER, 1000, emptyList())),
        row(PetOwnerResponse("Kate", "unknown", -1, null), PetOwner("Kate", Gender.OTHER, -1, emptyList()))
    )

    init {

        test("map response to PetOwner object") {
            table.forAll { response, expected ->
                mapToPetOwner(response) shouldBe expected
            }
        }
    }
}