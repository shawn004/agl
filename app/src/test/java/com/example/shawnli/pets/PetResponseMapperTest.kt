package com.example.shawnli.pets

import com.example.shawnli.pets.data.mappers.mapToPet
import com.example.shawnli.pets.data.responses.PetResponse
import com.example.shawnli.pets.domain.models.Pet
import com.example.shawnli.pets.domain.models.PetType
import io.kotlintest.shouldBe
import io.kotlintest.specs.FunSpec
import io.kotlintest.tables.Headers2
import io.kotlintest.tables.forAll
import io.kotlintest.tables.row
import io.kotlintest.tables.table

class PetResponseMapperTest : FunSpec() {

    private val table = table(
        Headers2("response", "expected"),
        row(PetResponse("A", "cat"), Pet("A", PetType.CAT)),
        row(PetResponse("B", "dog"), Pet("B", PetType.DOG)),
        row(PetResponse("C", "fish"), Pet("C", PetType.FISH)),
        row(PetResponse("D", "bird"), Pet("D", PetType.OTHER)),
        row(PetResponse("E", ""), Pet("E", PetType.OTHER))
    )

    init {

        test("map response to Pet object") {
            table.forAll { response, expected ->
                mapToPet(response) shouldBe expected
            }
        }
    }
}