package com.example.shawnli.pets

import com.example.shawnli.pets.data.managers.PetOwnerManager
import com.example.shawnli.pets.data.responses.PetOwnerResponse
import com.example.shawnli.pets.data.responses.PetResponse
import com.example.shawnli.pets.data.sources.PetOwnerClient
import com.example.shawnli.pets.domain.models.Gender
import com.example.shawnli.pets.domain.models.Pet
import com.example.shawnli.pets.domain.models.PetOwner
import com.example.shawnli.pets.domain.models.PetType
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.kotlintest.specs.FunSpec
import io.reactivex.Single

class PetOwnerManagerTest : FunSpec() {

    private val petOwnerResponses = listOf(
        PetOwnerResponse(
            name = "Peter",
            gender = "male",
            age = 50,
            pets = listOf(
                PetResponse(name = "Pie", type = "bird"),
                PetResponse(name = "Noodle", type = "dog")
            )
        ),
        PetOwnerResponse(
            name = "Selina",
            gender = "female",
            age = 30,
            pets = listOf(
                PetResponse(name = "Beer", type = "cat"),
                PetResponse(name = "Butter", type = "fish")
            )
        ),
        PetOwnerResponse(
            name = "Bob",
            gender = "unknown",
            age = 100,
            pets = null
        )
    )

    private val expectedResult = listOf(
        PetOwner(
            name = "Peter",
            gender = Gender.MALE,
            age = 50,
            pets = listOf(
                Pet(name = "Pie", type = PetType.OTHER),
                Pet(name = "Noodle", type = PetType.DOG)
            )
        ),
        PetOwner(
            name = "Selina",
            gender = Gender.FEMALE,
            age = 30,
            pets = listOf(
                Pet(name = "Beer", type = PetType.CAT),
                Pet(name = "Butter", type = PetType.FISH)
            )
        ),
        PetOwner(
            name = "Bob",
            gender = Gender.OTHER,
            age = 100,
            pets = emptyList()
        )
    )

    init {

        test("get a list of responses and map them to PetOwner objects") {
            // given
            val clientMock: PetOwnerClient = mock {
                on(mock.getPetOwners()) doReturn Single.just(petOwnerResponses)
            }
            val manager = PetOwnerManager(clientMock)

            // when
            val testObserver = manager.getPetOwners().test()
            testObserver.awaitTerminalEvent()

            // then
            testObserver
                .assertNoErrors()
                .assertValue(expectedResult)
        }
    }
}