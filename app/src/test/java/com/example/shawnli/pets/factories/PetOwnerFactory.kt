package com.example.shawnli.pets.factories

import com.example.shawnli.pets.factories.PetFactory.Companion.createPet
import com.example.shawnli.pets.domain.models.Gender
import com.example.shawnli.pets.domain.models.Pet
import com.example.shawnli.pets.domain.models.PetOwner

class PetOwnerFactory {

    companion object {
        fun create(
            name: String = "Mike",
            gender: Gender = Gender.MALE,
            age: Int = 20,
            pets: List<Pet> = listOf(createPet())
        ) =
            PetOwner(name, gender, age, pets)
    }
}

